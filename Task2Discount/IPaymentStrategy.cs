﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Task2Discount
{
    internal interface IPaymentStrategy
    {
        void PaymentProcess(decimal amount);
    }
}
