﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Task2Discount
{
    internal class PaymentProcessor
    {
        private readonly IPaymentStrategy paymentStrategy;
        private readonly ILogger logger;

        public PaymentProcessor(IPaymentStrategy paymentStrategy, ILogger logger)
        {
            this.paymentStrategy = paymentStrategy;
            this.logger = logger;
        }

        public void PaymentProcess(decimal amount)
        {
            // Log the payment process
            logger.Log("Processing payment...");

            // Execute the payment using the selected payment strategy
            paymentStrategy.PaymentProcess(amount);

            // Log the payment completion
            logger.Log("Payment processed.");
        }
    }
}
