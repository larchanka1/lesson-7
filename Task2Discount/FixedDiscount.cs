﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Task2Discount
{
    public class FixedDiscount : DiscountStrategy //Fixed discount with logic
    {
        private double fixedAmount;

        public FixedDiscount(double fixedAmount)
        {
            this.fixedAmount = fixedAmount;
        }
        public override double ApplyDiscount(double originalPrice)
        {
            return originalPrice - fixedAmount;
        }
    }
}
