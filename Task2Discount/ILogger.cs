﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Task2Discount
{
    internal interface ILogger
    {
        void Log(string message);
    }

    
    public class ConsoleLogger : ILogger //Console logger 
    {
        public void Log(string message) 
        {
            Console.WriteLine(message);
        }
    }

   
    public class FileLogger : ILogger  //File logger
    {
        private string filePath;

        public FileLogger(string filePath) 
        {
            this.filePath = filePath;
        }

        public void Log(string message) // Log to a file and output a message to the console
        {
            using (StreamWriter writer = File.AppendText(filePath))
            {
                writer.WriteLine(message);
            }
            Console.WriteLine("Logging: "+ filePath);

        }
    }
}
