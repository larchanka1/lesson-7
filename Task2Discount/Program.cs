﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Task2Discount
{
    class Program
    {
        static void Main(string[] args)
        {
            Product[] products = new Product[]
            {
                new Product("Sweater", 50),
                new Product("Jeans", 30),
            };

            var cart = new ShoppingCart(products); //Creating a shopping cart with array of products

            var fixedDiscount = new FixedDiscount(10.0);
            var percentageDiscount = new PercentageDiscount(20.0);

            cart.SetDiscountStrategy(fixedDiscount);
            double totalWithFixedDiscount = cart.CalculateTotal();
            Console.WriteLine("Total amount with Fixed Discount: $" + totalWithFixedDiscount);

            cart.SetDiscountStrategy(percentageDiscount);
            double totalWithPercentageDiscount = cart.CalculateTotal();
            Console.WriteLine("Total amount with percentage discount: $"+ totalWithPercentageDiscount);

            ILogger consoleLogger = new ConsoleLogger();

            IPaymentStrategy creditCardPayment = new CreditCardPaymentStrategy();

            var paymentProcessor = new PaymentProcessor(creditCardPayment, consoleLogger);

            var purchase1 = new Purchase("Product A", 50);
            var purchase2 = new Purchase("Product B", 30);

            paymentProcessor.PaymentProcess(purchase1.Amount);
            paymentProcessor.PaymentProcess(purchase2.Amount);

        }
    }
}
