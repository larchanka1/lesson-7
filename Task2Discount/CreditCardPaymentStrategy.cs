﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Task2Discount
{
    internal class CreditCardPaymentStrategy : IPaymentStrategy
    {
        public void PaymentProcess(decimal amount)
        {
            Console.WriteLine($"Paid " + amount + "via credit card.");
        }
    }
}
