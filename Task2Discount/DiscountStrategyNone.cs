﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Task2Discount
{
    public class DiscountStrategyNone : DiscountStrategy //No discount logic
    {
        public override double ApplyDiscount(double originalPrice)
        {
            return originalPrice;
        }
    }
}
