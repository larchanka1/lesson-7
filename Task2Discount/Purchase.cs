﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Task2Discount
{
    internal class Purchase
    {
        public string ProductName { get; }
        public decimal Amount { get; }
        public Purchase(string productName, decimal amount) 
        {
            ProductName = productName;
            Amount = amount;
        }
    }
}
