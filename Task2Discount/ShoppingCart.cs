﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Task2Discount
{
    public class ShoppingCart //ShoppingCart class is created
    {
        private Product[] products;
        private DiscountStrategy discountStrategy;

        public ShoppingCart(Product[] products)
        {
            this.products = products;
            discountStrategy = new DiscountStrategyNone(); //link to logic if no discount
        }

        public void SetDiscountStrategy (DiscountStrategy strategy) 
        {
            discountStrategy = strategy;
        }

        public double CalculateTotal()
        {
            double total = 0;
            foreach (var product in products)
            {
                total += discountStrategy.ApplyDiscount(product.Price);
            }
            return total;
        }
    }
}
