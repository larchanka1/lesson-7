﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Task2Discount
{
    public class PercentageDiscount : DiscountStrategy //Percentage discount with logic
    {
        private double percentage;

        public PercentageDiscount(double percentage)
        {
            this.percentage = percentage;
        }

        public override double ApplyDiscount(double originalPrice)
        {
            return originalPrice - (originalPrice * percentage / 100);
        }
    }
}
