﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Task1PetShop
{
    internal class Add
    {
        static void Main()
        {
            Animal[] animals = new Animal[]
            {
                new Dog(),
                new Cat()
            };

            PetShop petShop = new PetShop(animals);

            //Make animals make their noise
            petShop.MakeAllSounds();
        }
    }
}
