﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Task1PetShop
{
    public class PetShop
    {
        private Animal[] animals;

        public PetShop(Animal[] animals)
        {
            this.animals = animals;
        }

        public void MakeAllSounds()
        {
            Console.WriteLine("Sounds of animals in a pet shop: ");
            foreach (var animal in animals)
            {
                animal.MakeSound();
            }
        }
    }
}